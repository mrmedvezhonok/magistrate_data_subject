import random

from PIL import Image, ImageDraw, ImageFont, ImageOps

background_colors = [(46, 139, 87), (0, 139, 139), (70, 130, 180), (165, 42, 42)]
font_colors = font_colors = [(173, 255, 47), (255, 255, 0), (255, 255, 224), (0, 255, 255), (238, 130, 238), (255, 20, 147), (255, 248, 220)]

window_w, window_h = 600, 600
word_limit = 25
font_name = 'Lobster-Regular.ttf'

# text = 'Привет друг как тебе такое Илон МАСК Ох хох хошеньки!'
# text = 'Hmm what about english text???'

if __name__ == '__main__':
    text = input('Введите текст для генерации облака тэгов: ')
    words = text.replace('.', ' ').replace(',', ' ').replace('!', ' ').replace('?', ' ').replace('/', ' ')\
        .replace('+', ' ').replace('-', ' ').replace('(', ' ').replace(')', ' ').split()
    words = list(filter(lambda l_arg: bool(l_arg.strip()), words))

    main_word = random.choice(words)
    words = set(words) - set(main_word)

    bg_color = random.choice(background_colors)
    img = Image.new('RGB', (window_w, window_h), color=bg_color)

    canvas = ImageDraw.Draw(img)
    for i, word in enumerate(words):
        if i > word_limit: break

        font_size = random.randint(15, 35)
        font = ImageFont.truetype(font_name, font_size)
        font_color = random.choice(font_colors)

        x, y = random.randint(5, int(window_w * 0.9)), random.randint(1, window_h)

        canvas.text((x, y), word, fill=font_color, font=font)

    # INPUT MAIN VERTICAL WORD
    large_font = ImageFont.truetype(font_name, 50)
    font_color = random.choice(font_colors)
    pos = (30, window_h//2)
    main_text_img = Image.new('L', (window_w, window_h))
    d = ImageDraw.Draw(main_text_img)
    d.text((window_w//4, 0), "Sergey Khitrin", font=large_font, fill=255)
    w = main_text_img.rotate(90,  expand=1)
    img.paste(ImageOps.colorize(w, (0, 0, 0), (255, 255, 84)), (242, 60),  w)

    img.save('tag_cloud.png')
